/*
TicTacToe
Copyright (C) 2023  Jason Gibson

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; version 2.0
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.

License URL - https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
*/

#include <stdio.h>
#include <stdbool.h>
#include <ncurses.h>

extern bool player_cell_select(char * board, char p);

int main()
{
  // declare & initialize
  bool keepgoing = true;
  char board[10] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
  // loop each player till winner
  while (keepgoing == true) {
    if (player_cell_select(board, 'X') == true) {
      player_cell_select(board, 'O');
    } else {
      player_cell_select(board, 'X');
    }
  }
  return 0;
}
