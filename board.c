/*
TicTacToe
Copyright (C) 2023  Jason Gibson

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; version 2.0
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.

License URL - https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
*/

#include <stdio.h>

void print_board(char * board)
{
  printf("%s", "Welcome to Ghetto Tic Tac Toe!\n\n");
  printf("    |   |\n");
  printf("  %c | %c | %c\n", board[1], board[2], board[3]);
  printf("    |   |\n");
  printf(" _____________\n");
  printf("    |   |\n");
  printf("  %c | %c | %c\n", board[4], board[5], board[6]);
  printf("    |   |\n");
  printf(" _____________\n");
  printf("    |   |\n");
  printf("  %c | %c | %c\n", board[7], board[8], board[9]);
  printf("    |   |\n");
}
