#TicTacToe
#Copyright (C) 2023  Jason Gibson

#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; version 2.0
#of the License.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#02110-1301, USA.

#License URL - https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html


# requires lib-jmgeneral

# default target

.DEFAULT_GOAL := build

# variables

TAR = /usr/local/bin
BIN = tictactoe
LIB = jmgeneral

build:
	@clear
	@echo
	@echo "compiling and building executable file"
	@gcc -o ${BIN} -Wall -Werror -O3 *.c -l${LIB} -lncurses
	@echo "executable built"
	@echo

install:
	@clear
	@echo
	@install -d ${TAR}/
	@install -m 755 ${BIN} ${TAR}/
	@echo "installed executable"
	@echo

uninstall:
	@clear
	@echo
	@rm ${TAR}/${BIN}
	@echo "installed files removed"
	@echo

clean:
	@clear
	@echo
	@rm ./${BIN}
	@echo "directory cleaned for next compilation"
	@echo

