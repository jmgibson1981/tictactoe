/*
TicTacToe
Copyright (C) 2023  Jason Gibson

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; version 2.0
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.

License URL - https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <jmgeneral.h>

extern void print_board();
static void win_check(char * board, char p, int c1, int c2, int c3);
static void tie_check(char * board);

bool player_cell_select(char * board, char p)
{
  clear_stdout();

  // declare & initialize
  char a = '0'; // cell selected
  int b = 0; // int version of a
  bool retval = false;

  // reprint for each player turn
  print_board(board);

  // prompt for selection and set int of selection
  printf("it is %c's turn.\n", p);
  printf("enter a cell. 1-9. --> ");
  if (scanf(" %c", &a)) {
    b = atoi(&a);
  }

  // if cell isn't X or O then fill in with proper player
  if (board[b] == a) {
    board[b] = p;
    retval = true;
  } else {
    retval = false;
  }

  // test winning lines
  // horizontals
  win_check(board, p, 1, 2, 3);
  win_check(board, p, 4, 5, 6);
  win_check(board, p, 7, 8, 9);

  // verticals
  win_check(board, p, 1, 4, 7);
  win_check(board, p, 2, 5, 8);
  win_check(board, p, 3, 6, 9);

  // diagonals
  win_check(board, p, 1, 5, 9);
  win_check(board, p, 3, 5, 7);

  // verify all boxes filled if no winner yet
  tie_check(board);

  // retval based on whether or not success of cell change
  return(retval);
}

static void win_check(char * board, char p, int c1, int c2, int c3)
{
  // if all 3 characters in a line are the same then win
  if (board[c1] == board[c2] &&
      board[c2] == board[c3]) {
    clear_stdout();
    print_board(board);
    printf("%c Wins!\n", p);
    exit(0);
  }
}

static void tie_check(char * board)
{
  // count digits left on board
  int tiecount = 0;
  for (int i = 9; i >= 0; i--) {
    if (isdigit(board[i]) == false) {
      tiecount++;
    }
  }

  // if no digits then game over with tie
  if (tiecount == 9) {
    printf("tie game. sorry try again!\n");
    exit(0);
  }
}
